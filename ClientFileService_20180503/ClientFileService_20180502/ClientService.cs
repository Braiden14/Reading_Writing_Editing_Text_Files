﻿using FileService;
using System;
using System.Linq;
using TextFileProcessor;

namespace ClientFileService_20180502
{
    class ClientService
    {
        private const int Add = 1;
        private const int Edit = 2;
        private const int Delete = 3;
        private static IFileServices _fileService;
        private static IFileProcessor _fileProcessor;

        public static void Initialize(string filePath, string template)
        {
            _fileProcessor = new FileProcessor(filePath, template);
            _fileService = new FileServices(_fileProcessor);
        }

        static void Main(string[] args)
        {
            const string defaultFilePath = @"E:\Text Files\PeronalDetails.txt";
            const string defaultTemplate = @"E:\Text Files\PeronalDetailsTemplate.txt";

            Initialize(args.Length > 0 ? args[0] : defaultFilePath, args.Length > 0 ? args[1] : defaultTemplate);
            SetConsoleAppColors();

            while (true)
            {
                RunApp();
            }
        }

        private static void SetConsoleAppColors()
        {
            Console.BackgroundColor = ConsoleColor.DarkGray;
            Console.Title = "Personal Information";
        }

        private static void RunApp()
        {
            DisplayRecords();

            Console.WriteLine("\n\nOptions");
            Console.WriteLine("1>Add New");
            Console.WriteLine("2>Edit Information");
            Console.WriteLine("3>Delete Record");
            var userOption = Convert.ToInt32(Console.ReadLine());

            GetClientOption(userOption);
        }

        private static void DisplayRecords()
        {
            foreach (var line in _fileService.ReadFile())
            {
                Console.WriteLine($"{line}");
            }
        }

        private static void GetClientOption(int option)
        {
            switch (option)
            {
                case Add:
                    {
                        var userInput = GetUserInput();

                        WriteUserInput(userInput);
                        break;
                    }

                case Edit:
                    {
                        Console.WriteLine("\n\nEnter In the Information Entry Number you would like to edit");

                        var entryNumber = Convert.ToInt32(Console.ReadLine());

                        EditRecord(entryNumber);
                        break;
                    }

                case Delete:
                    {
                        Console.WriteLine("\n\nEnter In the Information Entry Number you would like to delete");

                        var entryNumber = Convert.ToInt32(Console.ReadLine());

                        DeleteRecord(entryNumber);
                        break;
                    }

                default:
                    Console.WriteLine("\n\nInvalid Input");
                    break;
            }
        }

        public static string[] GetUserInput()
        {
            var input = new string[8];
            var textFileLines = _fileService.ReadTemplate();

            for (var i = 0; i < 8; i++)
            {
                Console.WriteLine(textFileLines[i]);
                input[i] = textFileLines[i] + Console.ReadLine();
            }

            return input;
        }

        private static void WriteUserInput(string[] input)
        {
            _fileService.WriteToTextFile(input);
        }

        private static void EditRecord(int entryNumber)
        {
            Console.WriteLine("\nClick Enter if no changes needed for the field you on\n");

            var lines = _fileService.ReadFile();
            var userInput = GetUserInput();

            for (var i = 0; i < 8; i++)
            {
                var index = userInput[i].IndexOf(':');

                if (userInput[i].Length != index + 1)
                    lines[entryNumber * 8 + i] = userInput[i];
            }

            _fileService.UpdateRecord(lines);
        }

        private static void DeleteRecord(int entryNumber)
        {
            var lineList = _fileService.ReadFile().ToList();
            lineList.RemoveRange(entryNumber * 8, 8);

            _fileService.UpdateRecord(lineList.ToArray());
        }
    }
}
﻿namespace TextFileProcessor
{
    public interface IFileProcessor
    {
        string[] ReadFromTemplate();
        string[] ReadFromTextFile();
        void WriteToTextFile(string[] lines);
        void UpdateRecord(string[] lines);
    }
}

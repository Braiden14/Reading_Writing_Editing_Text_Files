﻿using System.IO;

namespace TextFileProcessor
{
    public class FileProcessor : IFileProcessor
    {
        private readonly string _filePath;
        private readonly string _templatePath;

        public FileProcessor(string filePath, string template)
        {
            _filePath = filePath;
            _templatePath = template;
        }

        public string[] ReadFromTemplate()
        {
            var lines = File.ReadAllLines(_templatePath);

            return lines;
        }

        public string[] ReadFromTextFile()
        {
            var lines = File.ReadAllLines(_filePath);

            return lines;
        }

        public void WriteToTextFile(string[] lines)
        {
            var streamWriter = new StreamWriter(_filePath, true);

            foreach (var line in lines)
            {
                streamWriter.WriteLine(line);
            }

            streamWriter.Close();
        }

        public void UpdateRecord(string[] lines)
        {
            var streamWriter = new StreamWriter(_filePath);

            foreach (var line in lines)
            {
                streamWriter.WriteLine(line);
            }

            streamWriter.Close();
        }
    }
}
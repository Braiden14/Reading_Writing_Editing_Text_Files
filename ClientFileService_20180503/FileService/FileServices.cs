﻿using TextFileProcessor;

namespace FileService
{
    public class FileServices : IFileServices
    {
        private readonly IFileProcessor _fileProcessor;

        public FileServices(IFileProcessor fileProcessor)
        {
            _fileProcessor = fileProcessor;
        }

        public string[] ReadTemplate()
        {
            return _fileProcessor.ReadFromTemplate();
        }

        public string[] ReadFile()
        {
            return _fileProcessor.ReadFromTextFile();
        }

        public void WriteToTextFile(string[] lines)
        {
            _fileProcessor.WriteToTextFile(lines);
        }

        public void UpdateRecord(string[] lines)
        {
            _fileProcessor.UpdateRecord(lines);
        }
    }
}
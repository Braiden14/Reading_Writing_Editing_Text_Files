﻿namespace FileService
{
    public interface IFileServices
    {
        string[] ReadTemplate();
        string[] ReadFile();
        void WriteToTextFile(string[] lines);
        void UpdateRecord(string[] lines);
    }
}
